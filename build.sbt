name := """pastiesmonitor"""
organization := "com.guereca"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.3"

resolvers += "Spark Packages Repo" at "https://dl.bintray.com/spark-packages/maven"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test


libraryDependencies += "com.datastax.cassandra" % "cassandra-driver-core" % "3.0.0"
libraryDependencies += "com.datastax.cassandra" % "cassandra-driver-mapping" % "3.0.0"

libraryDependencies += "datastax" % "spark-cassandra-connector" % "2.0.1-s_2.11"

libraryDependencies += "com.datastax.cassandra" % "cassandra-driver-extras" % "3.4.0"

libraryDependencies += "org.json4s" %% "json4s-native" % "3.6.0-M2"

libraryDependencies += "org.apache.kafka" % "kafka-streams" % "1.0.0"
libraryDependencies += "org.apache.kafka" % "kafka-clients" % "1.0.0"

libraryDependencies += "org.webjars" %% "webjars-play" % "2.6.0-M1"


// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.guereca.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.guereca.binders._"
