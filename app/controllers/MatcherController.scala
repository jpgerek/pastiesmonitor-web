package controllers

import java.util.{Properties, UUID}
import javax.inject._

import com.datastax.driver.core.utils.UUIDs
import com.datastax.driver.core.{Cluster, Row}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.json4s.NoTypeHints
import org.json4s.native.Serialization
import org.json4s.native.Serialization.write
import play.api.Configuration
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.I18nSupport
import play.api.mvc._

import scala.collection.JavaConversions._

case class RegexCount(regex: String, count: Int)
case class MatchingLogic(id:String, matchingRules:List[RegexCount], excludingRules:List[RegexCount])

@Singleton
class MatcherController @Inject()(cc: ControllerComponents, config: Configuration) extends AbstractController(cc) with I18nSupport {

  implicit val formats = Serialization.formats(NoTypeHints)

  def publish(producer: KafkaProducer[String, String], ml:MatchingLogic) = {
    implicit val formats = Serialization.formats(NoTypeHints)
    println("Matching logic added: " + ml.toString)
    val record = new ProducerRecord(config.get[String]("pastiesmon.topicNewMatchers"), "pastiesmon-web", write(ml))
    producer.send(record)
  }

  def getProducer():KafkaProducer[String, String] = {
    val  props = new Properties()
    props.put("bootstrap.servers", config.get[String]("pastiesmon.kafkaBootstrapServer"))
    props.put("client.id", "web-ui")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    return new KafkaProducer[String, String](props)
  }

  val kafkaProducer = getProducer()


  val cluster = Cluster.builder()
    .addContactPoints(config.get[String]("pastiesmon.CassandraHostname"))
    .withPort(config.get[Int]("pastiesmon.CassandraPort"))
    .build
  val session = cluster.connect(config.get[String]("pastiesmon.CassandraKeyspace"))

  val matcherForm = Form(
    tuple(
      "matching_regex" -> nonEmptyText,
      "matching_regex_count" -> number(min = 0, max = 99999),
      "excluding_regex" -> optional(nonEmptyText),
      "excluding_regex_count" -> optional(number(min = 0, max = 99999))
    )
  )

  def form() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.matcherForm(matcherForm))
  }

  def create() = Action { implicit request: Request[AnyContent] =>
      // XXX Validate

      val id = UUIDs.timeBased().toString()

      val params = request.body.asFormUrlEncoded.get

      val matchingRegex = params.get("matching_regex").getOrElse(Seq())
      val matchingRegexCount = params.get("matching_regex_count").getOrElse(Seq()).map(_.toInt)

      val excludingRegex = params.get("excluding_regex").filter(_=="").getOrElse(Seq())
      val excludingRegexCount = params.get("excluding_regex_count").filter(_=="").getOrElse(Seq()).map(_.toInt)

    /*
      // persist on cassandra

      val matchingRulesCassandra = matchingRegex.zip(matchingRegexCount).map((t:(String, Int)) => new TupleValue(t._1, t._2))

      println(matchingRulesCassandra)

      val excludingRulesCassandra = excludingRegex.zip(excludingRegexCount).map((t:(String, Int)) => new TupleValue(t._1, t._2))

      println(excludingRulesCassandra)

      val result = session.execute("INSERT INTO matchers(id_pk, created, matching_rules, excluding_rules) VALUES (?, ?, ?)", id, matchingRulesCassandra, excludingRulesCassandra)
    */
      // enqueue on kafka

      val matchingRules = matchingRegex.zip(matchingRegexCount).map((t:(String, Int)) => new RegexCount(t._1, t._2)).toList
      val excludingRules = excludingRegex.zip(excludingRegexCount).map((t:(String, Int)) => new RegexCount(t._1, t._2)).toList

      val ml = MatchingLogic(id, matchingRules, excludingRules)

      val kResult = publish(kafkaProducer, ml)

      Redirect(routes.MatcherController.get(id))
    }

    def get(id:String) = Action { implicit request: Request[AnyContent] =>
      // Get pasties list from cassandra
      val matchesResult = session.execute("SELECT pastie_url, toTimestamp(id_match_ck) as created FROM matches WHERE id_matcher_pk = ? ORDER BY id_match_ck DESC", UUID.fromString(id)).all()

      val pastiesURLs = matchesResult.map((t:Row) => (t.getString(0), t.getTimestamp(1))).toIterable

      val url = routes.MatcherController.get(id)
      val refreshValue = "60; url="+url.url
      Ok(views.html.matcher(pastiesURLs))
        .withHeaders("Refresh" -> refreshValue) // reload results every minute
    }
}